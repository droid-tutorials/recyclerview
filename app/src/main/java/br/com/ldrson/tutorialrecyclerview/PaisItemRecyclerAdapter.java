package br.com.ldrson.tutorialrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by leanderson on 02/09/16.
 */
public class PaisItemRecyclerAdapter extends RecyclerView.Adapter<PaisItemViewHolder> {

    private List<PaisItem> mLista;
    private Context mContext;

    public PaisItemRecyclerAdapter(Context context, List<PaisItem> lista){
        mLista = lista;
        mContext = context;
    }

    @Override
    public PaisItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pais, parent,false);
        return new PaisItemViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(PaisItemViewHolder holder, int position) {
        PaisItem item = mLista.get(position);
        holder.textNomeDoPais.setText(item.getNome());
        holder.textContinenteDoPais.setText(item.getContinente());
        Picasso.with(mContext).load(item.getUrlDaImagemDaBandeira()).into(holder.imageBandeiraDoPais);
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }

    public PaisItem getItem(int position){
        return mLista.get(position);
    }

}
