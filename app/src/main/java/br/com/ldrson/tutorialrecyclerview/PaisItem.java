package br.com.ldrson.tutorialrecyclerview;

import android.support.v7.widget.RecyclerView;

/**
 * Created by leanderson on 02/09/16.
 */
public class PaisItem {

    private String nome;
    private String continente;
    private String urlDaImagemDaBandeira;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public String getUrlDaImagemDaBandeira() {
        return urlDaImagemDaBandeira;
    }

    public void setUrlDaImagemDaBandeira(String urlDaImagemDaBandeira) {
        this.urlDaImagemDaBandeira = urlDaImagemDaBandeira;
    }
}
